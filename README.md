# GTAthen #

This is a game I made together with  Simen Heggestøyl for [Sonen Gamejam](http://sonengamejam.org/entries/) (Fall 2013). The theme was Geometry.
We wanted to set GTA to old greece times. The protagonist is Euclid, which despite his reputation, doesn't know shit
about geometry and math. In order to become one of the famous philosopher and mathematicians, he has to kill the others and
steal their work.

Unfortunately we ran low on time, as we in the middle of development decided to redo the whole game in pygame instead of Löve.
However our idea, intro and very simple gameplay resulted in us winning 2nd place on the gamejam.
from PIL import Image
import glob


maps = [Image.open(m) for m in sorted(glob.glob("assets/meps*.png"))]
mdata = [m.load() for m in maps]

level = 0
for data in mdata:
	#generate room based on image data
	w,h = (24,24)

	print 'layer' + str(level) + ' ='

	print '{'
	for y in range(0,h):
		print "{",

		for x in range(0,w):
			if data[x,y] == 20:
				print "'grass'",

			elif data[x,y] == 24:
				print "'cube'",

			elif data[x,y] == 28:
				print "'pillar'",

			elif data[x,y] == 31:
				print "'treetrunk'",

			elif data[x,y] == 14:
				print "'vase'",

			elif data[x,y] == 2:
				print "'statue0'",

			elif data[x,y] == 1:
				print "'statue1'",

			else:
				print "false",

			if x == w-1:
				print "}",
			else:
				print ",",

		if y != h-1:
			print ","

	print '}'

	level += 1

print "world = {layer0, layer1, layer2}"
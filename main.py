import pygame
from pygame import *
from functions import *
from anim import *
from player import *

def main():
	#init pygame
	pygame.init()
	pygame.font.init()
	
	#init display
	screen = display.set_mode((800,600), 0, 16)
	preScreen = Surface((int(800/3), int(600/3)))

	display.set_caption("GTAthen")

	font = pygame.font.SysFont("SuperAwesomeHyperUltraGeniousFont",20)
	timer = time.Clock()

	#bg
	background = pygame.Surface(screen.get_size())
	background = background.convert()
	background.fill((0, 0, 0))

	player = Player()

	right = left = up = down = False

	pt = 0

	while 1:
		#timer stuff
		#timer.tick(60)
		t = pygame.time.get_ticks()
		dt = t - pt
		pt = t

		#Handle keyevents
		for e in pygame.event.get():
			if e.type == QUIT:
				raise SystemExit, "QUIT"

			if e.type == KEYDOWN:
				if e.key == K_ESCAPE: 
					raise SystemExit, "Quit"

				if e.key == K_w:
					up = True

				if e.key == K_s:
					down = True

				if e.key == K_a:
					left = True

				if e.key == K_d:
					right = True

			if e.type == KEYUP:
				if e.key == K_w:
					up = False

				if e.key == K_s:
					down = False

				if e.key == K_a:
					left = False

				if e.key == K_d:
					right = False

		#update
		player.update(right,left,up,down, dt)

		#draw
		preScreen.blit(background, (0, 0))
		draw = player.draw()
		preScreen.blit(draw[0], draw[1])

		pygame.transform.scale(preScreen, (800,600), screen)
		
		pygame.display.flip()

main()

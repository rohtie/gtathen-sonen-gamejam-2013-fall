from world import *

tilew = 16 # Tile width in pixels
tileh = 8  # Tile height in pixels

# coords to pixels
def c2p(cx, cy):
	px = (cx - cy) * tilew
	py = (cy + cx) * tileh
	return (px, py)

# position in coordinate system to closest tile
def coords(x, y):
   cx = int(x)
   cy = int(y)
   return (cx, cy)

# highest point of map
def peak(x,y):
	print x,y
	for z in range(len(world)-1, -1, -1):
		if world[z][y][x]:
			return z
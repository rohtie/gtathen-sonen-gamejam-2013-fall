layer1 = [['cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube'],
    ['cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube'],
    ['cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube'],
    ['cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube'],
    ['cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube'],
    ['cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube'],
    ['cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube'],
    ['cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube'],
    ['cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube', 'cube']]

layer2 = [[False, 'cube', 'cube', False, 'cube', 'cube', 'cube', 'cube', False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, 'grass', 'grass', 'grass', False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False]]

layer3 = [[False, 'cube', False, False, False, 'cube', 'cube', 'cube', False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, 'grass', False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False]]

layer4 = [[False, 'cube', False, False, False, False, 'cube', 'cube', False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False],
    [False, False, False, False, False, False, False, False, False]]

world = [layer1, layer2, layer3, layer4]
local assets = {anims = {}}

function assets.load(dir)
   local files = love.filesystem.getDirectoryItems(dir)
   
   for i, file in ipairs(files) do
      local path = dir .. '/' .. file
      
      if love.filesystem.isDirectory(path) then -- Animation folder
         local frames = love.filesystem.getDirectoryItems(path)
         local name = string.sub(frames[1], 0, #(frames[1]) - 5)
         assets.anims[name] = {}
         for i, frame in ipairs(frames) do
            -- local num = string.sub(frame, #frame - 4, #frame - 4)
            local anim = love.graphics.newImage(path .. '/' .. frame)
            anim:setFilter("nearest", "nearest")
            assets.anims[name][i] = anim
         end
      elseif (string.sub(file, #file - 3, #file) == '.png') then -- PNG file
         local name = string.sub(file, 0, #file - 4)
         local asset = love.graphics.newImage(path)
         asset:setFilter("nearest", "nearest")
         assets[name] = asset
      end
   end
end

return assets

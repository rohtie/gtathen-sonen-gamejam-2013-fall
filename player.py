import pygame
from pygame import *
from functions import *
from anim import *
#from shadow import *

class Player(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)

		self.x = 0
		self.y = 0
		self.z = 0

		self.vx = 0
		self.vy = 0

		self.speed = 6
		self.slowness = 2
		self.friction = 3

		self.face = 'd'

		#idles
		self.idle = pygame.image.load("assets/player.png")
		self.idle_back = pygame.image.load("assets/player_back.png")

		#anims
		self.walk = Animation("player_walk")
		self.walk_back = Animation("player_walk_back")
		self.shoot_anim = Animation("player_shoot")
		self.shoot_back_anim = Animation("player_shoot")

		self.shoot = False

		#self.shadow = Shadow()

	def update(self, right, left, up, down, dt):
		if right:
			self.vx = min(self.vx + self.speed - self.slowness, self.speed)
			self.face = 'r'

		if left:
			self.vx = max(self.vx - self.speed - self.slowness, -self.speed)
			self.face = 'l'

		if down:
			self.vy = min(self.vy + self.speed - self.slowness, self.speed)
			self.face = 'd'

		if up:
			self.vy = max(self.vy - self.speed - self.slowness, -self.speed)
			self.face = 'u'

		if self.vx > 0:
			self.vx = max(self.vx - self.friction, 0)

		else:
			self.vx = min(self.vx + self.friction, 0)

		if self.vy > 0:
			self.vy = max(self.vy - self.friction, 0)

		else:
			self.vy = min(self.vy + self.friction, 0)

		dt /= 800.0

		newx = self.x + self.vx * dt
		newy = self.y + self.vy * dt

		#cx, cy = coords(newx, newy)

		# if (peak(cx, cy) - self.z) < 2:S
		# 	self.z = peak(cx, cy)

		self.x = newx
		self.y = newy

		# if self.shoot > 0:
		# 	self.shoot = self.shoot + dt * 5

		# 	if self.shoot > 3:
		# 		self.shoot = 0

	def draw(self):
		x = self.x - 0.5 * self.z
		y = self.y - 0.5 * self.z

		#shadow.draw(x, y, 96)

		if self.shoot > 0:
			return (self.shoot_anim.next(True), c2p(x, y))

		elif (abs(self.vx + self.vy) > 0):
			return (self.walk.next(True), c2p(x, y))

		else:
			return (self.idle, c2p(x, y))
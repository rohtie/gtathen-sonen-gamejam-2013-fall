import pygame
from pygame import *
import glob

class Animation(pygame.sprite.Sprite):
	def __init__(self, id):
		
		#load images
		self.left = [ pygame.image.load(frame) for frame in sorted(glob.glob("./assets/" + id + "/*.png")) ]

		#make a flipped version
		self.right = [ pygame.transform.flip(frame, True, False) for frame in self.left ]

		self.frames = self.left

		self.frame = 0
		self.counter = 0

	#return current frame
	def next(self, right):
		if right:
			self.frames = self.right
		else:
			self.frames = self.left

		if self.counter % 5 == 0:
			self.frame += 1

			if self.frame > len(self.frames) -1:
				self.frame = 0
				self.counter = 0

		nextFrame = self.frames[self.frame]

		self.counter += 1

		return nextFrame

	#reset to first frame
	def reset(self):
		self.frame = 0
		self.counter = 0
